<?php


class Nc_Events_Manager_Register_Plugin_Settings_Page
{

    private string $pageID;
    private $plugin_data;

    /**
     * @param $key
     *
     * @return string
     */
    public function get_plugin_data($key): string {
        return $this->plugin_data[$key];
    }

    /**
     * __constructor.
     */
    public function __construct()
    {
        $this->plugin_data = get_option('neoweb-connector-events-manager');
        $this->pageID = 'neoweb-connector-events-manager-plugin-settings';
    }

    public function registerSettingsPageNotices() {
        if( function_exists('acf_add_local_field_group') ):
            acf_add_local_field_group(array(
                'key' => 'group_cal_notices',
                'title' => 'group_cal_notices',
                'fields' => array(
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 1,
                'position' => 'acf_after_title',
                'style' => 'seamless',
                'label_placement' => 'top',
                'instruction_placement' => 'field',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));

            acf_add_local_field(array(
                'key' => 'group_cal_fields_' . $this->get_plugin_data('pluginSlug'),
                'label' => '',
                'name' => 'group_cal_fields_' . $this->get_plugin_data('pluginSlug'),
                'type' => 'message',
                'message' => 'Please select which sections to include in the events calendar and then click "Update" to reveal/update the shortcode.',
                'parent' => 'group_cal_notices',
                'wrapper' => array (
                    'width' => '',
                    'class' => 'neowebNotice',
                    'id' => '',
                ),
            ));
        endif;
    }

    public function registerSettingsPageFields($resourceData, $sectionsMetaData) {
        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_cal_available_sections',
                'title' => 'Available Sections',
                'fields' => array(
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 10,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'field',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));

            acf_add_local_field(array(
                'key' => 'group_cal_refresh_fields_' . $this->get_plugin_data('pluginSlug'),
                'label' => 'Data on this page is refreshed every hour.<br/>Next scheduled refresh',
                'name' => 'group_cal_refresh_fields_' . $this->get_plugin_data('pluginSlug'),
                'type' => 'message',
                'message' =>  $sectionsMetaData,
                'parent' => 'group_cal_available_sections',
            ));

            foreach ($resourceData as $groupID => $groupSections) {
                acf_add_local_field(array(
                    'key' => 'group_cal_fields_accordion' . $this->get_plugin_data('pluginSlug') . '_' . $groupID,
                    'label' => $groupSections[0]['group_name'] . ' (Group ID: ' . $groupID . ')',
                    'name' => 'group_cal_fields_accordion' . $this->get_plugin_data('pluginSlug') . '_' . $groupID,
                    'type' => 'accordion',
                    'parent' => 'group_cal_available_sections',
                ));
                foreach ( $groupSections as $groupSection ) {
                    acf_add_local_field(array(
                        'key' => 'group_cal_fields_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'label' => 'Include this section',
                        'name' => 'group_cal_fields_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'type' => 'true_false',
                        'message' => $groupSection['group_name'] . ' - ' . $groupSection['section_name'],
                        'parent' => 'group_cal_available_sections',
                        'wrapper' => array (
                            'width' => '',
                            'class' => 'sectionHeading',
                            'id' => '',
                        ),
                    ));
                    acf_add_local_field(array(
                        'key' => 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'label' => '',
                        'name' => 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'type' => 'true_false',
                        'message' => 'Show a key for this section',
                        'parent' => 'group_cal_available_sections',
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'group_cal_fields_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                                    'operator' => '==',
                                    'value' => 1
                                ),
                            ),
                        ),
                    ));
                    acf_add_local_field(array(
                        'key' => 'group_cal_events_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'label' => 'Events',
                        'name' => 'group_cal_events_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'type' => 'color_picker',
                        'parent' => 'group_cal_available_sections',
                        'instructions' => 'Background Colour',
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                                    'operator' => '==',
                                    'value' => 1
                                ),
                            ),
                        ),
                    ));
                    acf_add_local_field(array(
                        'key' => 'group_cal_events_text_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'label' => '',
                        'name' => 'group_cal_events_text_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'type' => 'color_picker',
                        'parent' => 'group_cal_available_sections',
                        'instructions' => 'Text Colour',
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                                    'operator' => '==',
                                    'value' => 1
                                ),
                            ),
                        ),
                    ));
                    acf_add_local_field(array(
                        'key' => 'group_cal_meetings_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'label' => 'Meetings / Programme',
                        'name' => 'group_cal_meetings_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'type' => 'color_picker',
                        'parent' => 'group_cal_available_sections',
                        'instructions' => 'Background Colour',
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                                    'operator' => '==',
                                    'value' => 1
                                ),
                            ),
                        ),
                    ));
                    acf_add_local_field(array(
                        'key' => 'group_cal_meetings_text_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'label' => '',
                        'name' => 'group_cal_meetings_text_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                        'type' => 'color_picker',
                        'parent' => 'group_cal_available_sections',
                        'instructions' => 'Text Colour',
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'show_key_' . $this->get_plugin_data('pluginSlug') . "_" . $groupSection['section_id'],
                                    'operator' => '==',
                                    'value' => 1
                                ),
                            ),
                        ),
                    ));

                }
            }


        endif;

    }

    public function registerSettingsPageShortCodes() {
        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_cal_shortcode',
                'title' => 'Event & Program Calendar Shortcode',
                'fields' => array(
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 5,
                'position' => 'acf_after_title',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'field',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));

            acf_add_local_field(array(
                'key' => 'group_cal_generated_shortcode' . $this->get_plugin_data('pluginSlug'),
                'label' => '',
                'name' => 'group_cal_generated_shortcode' . $this->get_plugin_data('pluginSlug'),
                'type' => 'text',
                'wrapper' => array(
                    'class' => 'shortCodeCopy',
                ),
                'readonly' => 1,
                'default_value' => '[OSM_Group_Calendar sections=\'NO SECTIONS SELECTED\']',
                'parent' => 'group_cal_shortcode',
            ));
        endif;
    }

}