<?php


use utils\NeoWeb_Connector_Auth_Caller;
use utils\NeoWeb_Connector_OSM_End_Points;

class Nc_Events_Manager_Register_Plugin_Short_Codes
{
    /**
     * @var array
     */
    private $plugin_data;

    /**
     * @param $key
     *
     * @return string
     */
    public function get_plugin_data($key): string {
        return $this->plugin_data[$key];
    }

    /**
     * __constructor
     *
     * @since    1.0.0
     */
    public function __construct( ) {
        $this->plugin_data = get_option('neoweb-connector-events-manager');
    }

    public function fetch_group_event_program_data ($attr, $sectionIDs): string {

        $eventsArray = array();
        foreach (json_decode($sectionIDs) as $sectionID) {

            $currentTermID = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getCurrentTermID($sectionID);

            if ($currentTermID != "") {
                $url = NeoWeb_Connector_OSM_End_Points::getAllEventData;
                $formattedURL = (new NeoWeb_Connector_OSM_End_Points())->formatEndPoint($url, $sectionID, $currentTermID);

                $transientID = 'allEventData_' . $sectionID;
                $eventsData = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osmAPICaller($transientID, $formattedURL, 48, array());

                if (is_object($eventsData) && is_array($eventsData)) {
                    foreach ($eventsData['items'] as $event) {
                        $startDate = $event['startdate'];
                        $formatStartDate = str_replace('/', '-', $startDate);

                        $endDate = $event['enddate'];
                        $formatEndDate = str_replace('/', '-', $endDate);

                        $eventData = array(
                            'id'                => $event['eventid'],
                            'title'             => $event['name'],
                            'start'             => date('Y-m-d', strtotime($formatStartDate)) . " " . $event['starttime'],
                            'end'               => date('Y-m-d', strtotime($formatEndDate)) . " " . $event['endtime'],
                            'backgroundColor'   => (get_field('group_cal_events_key_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') ? get_field('group_cal_events_key_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') : "#7b9b21"),
                            'textColor'         => (get_field('group_cal_events_text_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') ? get_field('group_cal_events_text_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') : "#FFFFFF"),
                            'groupName'         => (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getSectionGroupName($sectionID),
                            'sectionName'       => (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getSectionName($sectionID)
                        );
                        array_push($eventsArray, $eventData);
                    }
                }

                $url = NeoWeb_Connector_OSM_End_Points::getAllProgramData;
                $formattedURL = (new NeoWeb_Connector_OSM_End_Points())->formatEndPoint($url, $sectionID, $currentTermID);

                $transientID = 'allProgramData_' . $sectionID;
                $programData = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osmAPICaller($transientID, $formattedURL, 48, array());

                if (isset($programData['items']) &&
                    (is_array($programData['items']) || is_object($programData['items']))) {

                    foreach ($programData['items'] as $meeting) {

                        $meetingData = array(
                            'id'                => $meeting['eveningid'],
                            'title'             => $meeting['title'],
                            'start'             => $meeting['meetingdate'] . " " . $meeting['starttime'],
                            'end'               => $meeting['meetingdate'] . " " . $meeting['endtime'],
                            'backgroundColor'   => (get_field('group_cal_meetings_key_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') ? get_field('group_cal_meetings_key_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') : "#6b17d5"),
                            'textColor'         => (get_field('group_cal_meetings_text_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') ? get_field('group_cal_meetings_text_' . $this->get_plugin_data('pluginSlug') . "_" . $sectionID, 'option') : "#FFFFFF"),
                            'groupName'         => (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getSectionGroupName($sectionID),
                            'sectionName'       => (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getSectionName($sectionID)
                        );
                        array_push($eventsArray, $meetingData);
                    }
                }

            }
        }

        $calData = json_encode($eventsArray);
        $pluginSlug = $this->get_plugin_data('pluginSlug');
        $authCaller = new NeoWeb_Connector_Auth_Caller($this->getAllPluginData());
        $html = "";
        ob_start();
        include(get_option('neoweb-connector-events-manager-path') . 'public/partials/nc-events-manager-public-calendar.php' );
        $html .= ob_get_clean();

        return $html;

    }

    private function getAllPluginData()
    {
        return $this->plugin_data;
    }

}