# Change Log
##Version 1.3 (17/08/2021)
* Minor bug fixes, around licence timing out
##Version 1.2.1 (22/05/2021)
* Update api handler
* Remove need to clear licence after upgrade/error

##Version 1.2 (26/04/2021)
* Update cache manager
* Update powered by text

##Version 1.1.1 (22/03/2021)
* Fix short code generator bug

##Version 1.1
* Improve admin page load times
* Other minor bug fixes

##Version 1.0
Replacement for NeoWeb Authenticator Plugin adding oAuth support and splitting into separate plugins/add-ons