<style>
    /*word wrap on group calendar and colours on the key*/
    .fc-title {
        padding: 0 1px;
        white-space: normal;
    }

    .key{
        margin-top: 15px;
    }
    .keyevents  {
        background-color:#7b9b21;
        color:#ffffff;
        padding: 5px;
        margin-right: 10px;
    }
    .keymeetings  {
        background-color:#6b17d5;
        color:#ffffff;
        padding: 5px;
    }
</style>

<div id="NeoWeb_fullcalendar"></div>
<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small><?php echo (new Neoweb_Connector_Public())->showSomeLove(); ?></small></p>
<!-- The Modal -->
<div id="eventViewer" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-heading">
            <span class="close">Close window &times;</span>
            <h3>Group/District - Section: <span class="eventGroupSection"></span></h3>
        </div>
        <div class="modal-body">
            <p><strong>Meeting / Event title: </strong> <span class="eventTitle"></span></p>
            <p><strong>Start Time: </strong> <span class="eventStart"></span></p>
            <p><strong>End Time: </strong> <span class="eventEnd"></span></p>
        </div>
    </div>

</div>

<?php

foreach (json_decode($sectionIDs) as $sectionID) {

    if (get_field('show_key_' . $pluginSlug . "_" . $sectionID, 'option')) { ?>

    <div class="key">
        <?php echo $authCaller->getSectionGroupName($sectionID); ?> -
        <?php echo $authCaller->getSectionName($sectionID); ?> Key:
            <span class="keyevents"
                  style="background-color: <?php echo get_field('group_cal_events_key_' . $pluginSlug . "_" . $sectionID, 'option'); ?>;
                          color: <?php echo get_field('group_cal_events_text_' . $pluginSlug . "_" . $sectionID, 'option'); ?>;">
                Events
            </span>
            <span class="keymeetings"
                  style="background-color: <?php echo get_field('group_cal_meetings_key_' . $pluginSlug . "_" . $sectionID, 'option'); ?>;
                    color: <?php echo get_field('group_cal_meetings_text_' . $pluginSlug . "_" . $sectionID, 'option'); ?>;">
                Programme meetings
            </span>
    </div>
<?php }
}?>


<script>

    document.addEventListener('DOMContentLoaded', function() {
        const nw_connector_calendarEl = document.getElementById('NeoWeb_fullcalendar');
        const nw_connector_calendar = new FullCalendar.Calendar(nw_connector_calendarEl, {
            initialView: 'dayGridMonth',
            events: <?php echo $calData; ?>,
            eventClick: function(calEvent, jsEvent, view) {

                var modal = document.getElementById("eventViewer");
                modal.style.display = "block";

                jQuery('.eventGroupSection').text(calEvent.event.extendedProps.groupName + " - " + calEvent.event.extendedProps.sectionName);
                jQuery('.eventTitle').text(calEvent.event.title);

                const startDate = new Date(calEvent.event.start);
                jQuery('.eventStart').text(startDate.toLocaleString());

                const endDate = new Date(calEvent.event.end);
                jQuery('.eventEnd').text(endDate.toLocaleString());

            },
        });
        nw_connector_calendar.render();

        // Get the modal
        const modal = document.getElementById("eventViewer");

        // Get the <span> element that closes the modal
        const span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target === modal) {
                modal.style.display = "none";
            }
        }
    });

</script>