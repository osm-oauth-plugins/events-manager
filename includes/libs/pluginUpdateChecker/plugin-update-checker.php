<?php
/**
 * Plugin Update Checker Library 4.10
 * http://w-shadow.com/
 * https://github.com/YahnisElsts/plugin-update-checker
 *
 * Copyright 2020 Janis Elsts
 * Released under the MIT license. See license.txt for details.
 */

require dirname(__FILE__) . '/load-v4p10.php';